<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/xhtml1-strict.dtd">
<html>
 <head>
  <meta name="author" content="Rafael Diaz de Leon Plata" />
  <meta name="keywords" content="Linux, UANL, Monterrey, Mexico, GUL, Libre, Software " />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
<link rel="shortcut icon" href="img/ico_tux.gif" />
<style type="text/css">
@import url("gul.css");
</style>
<title>Grupo de Usuarios Linux de la UANL</title>
</head>
<body >
<img alt="GUL"align=right src="img/Logotrans1.png">
<div id="contenedor">
<div id="banner">
<h1 id="encabezado">#>GUL-UANL<span class="blink">_</span></H1>
<br />
<div id="menu">
<ul>
<li><a href="index.php?page=content">Principal</a></li>
<li><a href="index.php?page=juntas"> Juntas</a></li>
<li><a href="index.php?page=proy"> <span class="update" > Proyectos </span> </a></li>
<li><a href="index.php?page=descargas"><span class="update">Descargas</span> </a></li>
<li><a href="index.php?page=documentos">Documentos</a></li>
<li><a href="index.php?page=miembros"><span class="update">Miembros</span> </a></li>
<li><a href="index.php?page=ayuda">Ayuda</a></li>
</ul>
</div>
</div>
<div id="lista">
GULs de Monterrey
<ul>
<li><a href="http://linux.mty.itesm.mx/">GULTec</a></li>
<li><a href="http://www.guglitnl.org/">GUGLITNL</a></li>
<li><a href="http://linuxmonterrey.org/">Gulmty</a></li> 
<li><a href="http://www.gulur.org/">GulUR</a></li> 
</ul>

Canal de IRC:
<ul>
<li>#sentx</li>
</ul>
Servidor: irc.freenode.net
<div id="contenedor_reloj"></div>
</div>
<div id="contenido">
<?php
   setlocale(LC_ALL,es_MX);
   print "Facultad de Ciencias F&iacute;sico Matem&aacute;ticas";
   echo "<BR>\n";
   $page=$_GET['page'];
   if($page==""){
    $page="content";
   }
  require_once("$page.html"); 
    ob_start();
    $str = ob_get_contents();
  ?>
<br />
<br />
<span id="tiny"> Esta página funciona gracias a bebidas altas en azúcar.</span>
</div>
</body>
</html>
